/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class Lab1 {
    
    static ArrayList<Integer> playerPositions = new ArrayList<Integer>();
    static ArrayList<Integer> cpuPositions = new ArrayList<Integer>();

    public static void main(String[] args) {
        char[][] gameBoard = {{ ' ',  ' ',  ' ', '|', ' ', ' ', ' ', '|', ' ', ' ', ' ',},
            { ' ', '-', ' ', '+', ' ','-', ' ', '+', ' ', '-', ' '},
            { ' ',  ' ',  ' ', '|', ' ', ' ', ' ', '|', ' ', ' ', ' ',},
            { ' ', '-', ' ', '+', ' ','-', ' ', '+', ' ', '-', ' '},
            { ' ',  ' ',  ' ', '|', ' ', ' ', ' ', '|', ' ', ' ', ' ',}};
        Scanner scan = new Scanner(System.in);
        
            System.out.println("Start XO Game!!!");
        while (true) {  
                    
            System.out.print("Please input in Number (1-9): ");
        int playerPos = scan.nextInt();
        
            while (playerPositions.contains(playerPos)|| cpuPositions.contains(playerPositions)) {                
                System.err.println("Position taken! Enter a correct Position");
                playerPos = scan.nextInt();
                        
            }
        placePiece(gameBoard, playerPos, "player");
        
        Random rand = new Random();
        int cpuPos = rand.nextInt(9)+1;
        while (playerPositions.contains(cpuPos)|| cpuPositions.contains(cpuPos)) {               
                cpuPos = rand.nextInt(9)+1;
         
                     
            }
        
        placePiece(gameBoard, cpuPos, "cpu"); 
        printGameBoard(gameBoard);  
        
        String result = checkWinner();
        if(result.length() > 0) {
             System.out.println(result);
             break;
        }
        
        
           
        }
       
        
    }
    
        
    public static void printGameBoard(char [][] gameBoard) {
        
        for(char[] row : gameBoard) {
            for(char c : row) {
                System.out.print(c);
            }
            System.out.println();
        }
    }
    public static void placePiece(char[][] gameBoard, int pos, String user) {
        
        char symbol = 'X';
        
        if(user.equals("player")) {
            symbol = 'X';
            playerPositions.add(pos);
        } else if(user.equals("cpu")) {
            symbol = 'O';
            cpuPositions.add(pos);
            
        }
        switch (pos) {
            case 1: 
                gameBoard[0][1] = symbol;
                break;
            case 2: 
                gameBoard[0][5] = symbol;
                break;
            case 3: 
                gameBoard[0][9] = symbol;
                break;
            case 4: 
                gameBoard[2][1] = symbol;
                break;    
            case 5: 
                gameBoard[2][5] = symbol;
                break;    
            case 6: 
                gameBoard[2][9] = symbol;
                break;    
            case 7: 
                gameBoard[4][1] = symbol;
                break;
            case 8: 
                gameBoard[4][5] = symbol;
                break;
            case 9: 
                gameBoard[4][9] = symbol;
                break;    
            default:
                break;
                
        }
        
    }
    public static String checkWinner() {
        List topRow = Arrays.asList(1, 2, 3);
        List midRow = Arrays.asList(4, 5, 6);
        List botRow = Arrays.asList(7, 8, 9);
        List leftCol = Arrays.asList(1, 4, 7);
        List midCol = Arrays.asList(2, 5, 8);
        List rightCol = Arrays.asList(3, 6, 9);
        List cross1 = Arrays.asList(1, 5, 9);
        List cross2 = Arrays.asList(7, 5, 3);
        
        List<List> winnig = new ArrayList<List>();
        winnig.add(topRow);
        winnig.add(midRow);
        winnig.add(botRow);
        winnig.add(leftCol);
        winnig.add(midCol);
        winnig.add(rightCol);
        winnig.add(cross1);
        winnig.add(cross2);
        
        for(List i : winnig){
            if(playerPositions.containsAll(i)) {
                return "You Won!!!";
            }else if(cpuPositions.containsAll(i)){
                return "CPU wins!";
            }else if(playerPositions.size() + cpuPositions.size() == 9){
                return "Tie!!!";
            }
        }
        
        return "";
    }
}